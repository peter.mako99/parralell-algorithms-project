#include <mpi.h> //On windows, not sure on linux.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define size 10 //Actual length will be 2 on the power of <size>
#define max_limit 10000 //Defines maximum limit for random numbers
#define min_limit -10000 //Defines minimum limit for random numbers

int count_maximas(int rank, int length, int n, float array[]);

int main(int argc, char* argv[])
{
    int rank, ntasks, i, maxc, maxsum, tag1, tag2;
    double length, randomgen_start_time, randomgen_end_time, maxima_start_time, maxima_end_time;
    float* array_part;
    float* array;
    MPI_Status status;
    length = pow(2, size);

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

        tag1 = 2;
        tag2 = 1;
        srand((unsigned int)time(NULL) + rank);
        MPI_Alloc_mem(length * sizeof(float), MPI_INFO_NULL, &array);
        array_part = (float*)malloc(length / ntasks * sizeof(float));

        if (array_part == NULL)
        {
            printf("\nCouldn't allocate memory...");
            printf("\nTry a smaller size!\n");
            exit(0);
        }

        if (rank == 0) {
          //Generate Randoms without duplicates//
            randomgen_start_time = MPI_Wtime();
            for (i = 0; i < length/ntasks; i++) {
                array_part[i] = min_limit + (float)rand() / (float)RAND_MAX * (max_limit - (min_limit + 1));

                if (i > 0 && array_part[i] == array_part[i - 1]) {            
                    float num = 0;
                    while (num == 0) {
                        num = -1 + (float)rand() / (float)RAND_MAX * (1);
                    }
                    array_part[i] += num;
                }
            }
            MPI_Gather(array_part, length/ntasks, MPI_FLOAT, array, length / ntasks, MPI_FLOAT, 0, MPI_COMM_WORLD);
            free(array_part);
            randomgen_end_time = MPI_Wtime();
            printf("\n[Random Number Generator] Time elapsed: %.10lf seconds\n", randomgen_end_time - randomgen_start_time);

          //Check for consequent duplicates on the arraypart borders, if found change them//   
            for (i = length/ntasks-1; i < length; i += length / ntasks) {
                 if (array[i] == array[i + 1] && i<length-1) {
                     float num = 0;
                     while (num == 0) {
                         num = -1 + (float)rand() / (float)RAND_MAX * (1);
                     }
                     array[i] += num;
                 }
            }
          //Send the border duplicate free array to the other tasks for local maxima search//
            for (i = 1; i < ntasks; i++) {
                MPI_Send(array, length, MPI_FLOAT, i, tag2, MPI_COMM_WORLD);
            } 
          //Search for local maximas//  
            maxima_start_time = MPI_Wtime();
                maxc = count_maximas(rank, length, ntasks, array);
                MPI_Reduce(&maxc, &maxsum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
            maxima_end_time = MPI_Wtime();
            printf("[Local Maxima Searcher] Time elapsed: %.10lf seconds\n", maxima_end_time - maxima_start_time);
            printf("\nDone. Number of local maximas found: %d\n", maxsum);
        }

        if (rank > 0) {   
          //Generate Randoms without duplicates//
            for (i = 0; i < length / ntasks; i++) {
                array_part[i] = min_limit + (float)rand() / (float)RAND_MAX * (max_limit - (min_limit + 1));

                if (i > 0 && array_part[i] == array_part[i - 1]) {
                    float num = 0;
                    while (num == 0) {
                        num= -1 + (float)rand() / (float)RAND_MAX * (1);
                    }
                    array_part[i] += num;
                }
            }
            MPI_Gather(array_part, length / ntasks, MPI_FLOAT, array, length / ntasks, MPI_FLOAT, 0, MPI_COMM_WORLD);
            free(array_part);
          //Search for local maximas//  
            MPI_Recv(array, length, MPI_FLOAT, 0, tag2, MPI_COMM_WORLD, &status);
                maxc = count_maximas(rank, length, ntasks, array);
            MPI_Reduce(&maxc, &maxsum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);    
        }  
    MPI_Free_mem(array);
    MPI_Finalize();
}

int count_maximas(int rank, int length, int n, float array[]) {
    int i;
    int maxima_count;
    
    maxima_count = 0;
    for (i = rank + 1; i < length - 1; i = i + n) {
        if (i + 1 < length) {
            if (array[i - 1] <= array[i] && array[i] >= array[i + 1]) {
                maxima_count++;
            }
        }
    }
    return(maxima_count);
}
